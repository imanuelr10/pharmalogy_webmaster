<?php
/**
 * Created by PhpStorm.
 * User: nathanael79
 * Date: 12/11/18
 * Time: 9:10 PM
 */

namespace App;

class Produk extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'produk';
    public $timestamps = false;
    protected $fillable =
        [
            'no_obat',
            'nama',
            'harga_jual',
            'kategori_id',
            'supplier_id'
        ];
}
