<?php
/**
 * Created by PhpStorm.
 * User: nathanael79
 * Date: 12/7/18
 * Time: 8:16 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

    class ApiLogin extends Controller
    {
        public function login(Request $request)
        {
            $user = User::where('username',$request->username)->first();
            $password = sha1($request->password);
            if($user != null)
            {
                if($user->password == $password)
                {
                    return response()->json([
                        "status"=>true,
                        "code"=>200,
                        "data"=>$user,
                        "message"=>"berhasil login"
                    ]);
                }
                else
                {
                    return response()->json([
                        "status"=>false,
                        "code"=>500,
                        "message"=>"gagal login"
                    ]);
                }
            }
        }
    }
