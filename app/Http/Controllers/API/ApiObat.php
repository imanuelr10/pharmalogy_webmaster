<?php
/**
 * Created by PhpStorm.
 * User: nathanael79
 * Date: 12/11/18
 * Time: 9:13 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Produk;
use Illuminate\Support\Facades\DB;

class ApiObat extends Controller
{
    public function showObat()
    {
        //$obat = Produk::where('kategori_id',1)->get();

        $obat = DB::table('produk')
            ->join('stok_toko','produk.id','=','stok_toko.produk_id')
            ->where('produk.kategori_id','=','1')
            ->get();

        if($obat)
        {
            return response()->json([
                "status"=>true,
                "code"=>200,
                "message"=>"data berhasil ditampilkan",
                "data"=>$obat
            ]);
        }
        else
        {
            return response()->json([
                "status"=>false,
                "code"=>500,
                "message"=>"data gagal ditampilkan"
            ]);
        }
    }

}
