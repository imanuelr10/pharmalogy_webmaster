<?php
/**
 * Created by PhpStorm.
 * User: nathanael79
 * Date: 12/13/18
 * Time: 5:22 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class KategoriProduk extends Model
{
    protected $table = 'kategori_produk';
    public $timestamps = 'false';
    protected $fillable =
        [
          'nama'
        ];

}
