<?php
/**
 * Created by PhpStorm.
 * User: nathanael79
 * Date: 12/7/18
 * Time: 8:16 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ApiRegister extends Controller
{
    public function register(Request $request)
    {
        $token = str_random(100);
        $user = new User();
        $user->username = $request->username;
        $user->password  = sha1($request->password);
        $user->nama = $request->nama;
        $user->jenis_kelamin = $request->jenis_kelamin;
        $user->telepon = $request->telepon;
        $user->alamat = $request->alamat;
        $user->jabatan = $request->jabatan;
        $user->token = $token;
        if($user->save())
        {
            return response()->json([
                "status"=>true,
                "code"=>200,
                "message"=>"user berhasil dibuat"
            ]);
        }
        else
        {
            return response()->json([
                "status"=>false,
                "code"=>500,
                "message"=>"user gagal dibuat"
            ]);
        }
    }

}
