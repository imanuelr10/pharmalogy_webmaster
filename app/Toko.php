<?php
/**
 * Created by PhpStorm.
 * User: nathanael79
 * Date: 12/13/18
 * Time: 5:25 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{
    public $timestamps = false;
    protected $table = 'toko';
    protected $fillable =
        [
          'nama_toko',
          'alamat_toko',
          'tlp_toko'
        ];

}
