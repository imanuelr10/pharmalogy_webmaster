<?php
/**
 * Created by PhpStorm.
 * User: nathanael79
 * Date: 12/11/18
 * Time: 10:37 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $table = 'absensi';
    public $timestamps = false;
    protected $fillable =
        [
            'tanggal',
            'absen_masuk',
            'absen_keluar',
            'user_id'
        ];

}
