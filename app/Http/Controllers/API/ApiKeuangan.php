<?php
/**
 * Created by PhpStorm.
 * User: nathanael79
 * Date: 12/11/18
 * Time: 9:43 PM
 */

namespace App\Http\Controllers\API;


use App\DetailTransaksi;
use App\Http\Controllers\Controller;
use App\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiKeuangan extends Controller
{
    public function bulanKeuangan()
    {
        $tanggal = Transaksi::selectRaw('concat(year(tanggal), concat("-", month(tanggal))) as tanggal')->get();

        if($tanggal)
        {
            return response()->json([
                "status"=>true,
                "code"=>200,
                "message"=>"bulan berhasil ditampilkan",
                "data"=>$tanggal
            ]);
        }else
        {
            return response()->json([
                "status"=>false,
                "code"=>500,
                "message"=>"gagal berhasil ditampilkan"
            ]);
        }
    }

    public function Keuangan($tahun, $bulan)
    {
        //$idTransaksi = Transaksi::where('user_id',$user_id)->whereYear('tanggal',$tahun)->whereMonth('tanggal',$bulan)->count();
        $keuangan = DB::table('transaksi')
            ->join('detail_transaksi','transaksi.id','=','detail_transaksi.transaksi_id')
            ->whereYear('transaksi.tanggal','=',$tahun)
            ->whereMonth('transaksi.tanggal','=',$bulan)
            ->where('transaksi.category_transaksi','=','1')
            ->get();

        $keuanganTotal = DB::table('transaksi')
            ->join('detail_transaksi','transaksi.id','=','detail_transaksi.transaksi_id')
            ->whereYear('transaksi.tanggal','=',$tahun)
            ->whereMonth('transaksi.tanggal','=',$bulan)
            ->where('transaksi.category_transaksi','=','1')
            ->sum('detail_transaksi.total');

        if($keuangan)
        {
            return response()->json([
                "status"=>true,
                "code"=>200,
                "message"=>"id transaksi berhasil ditampilkan",
                "data"=>$keuangan,
                "sum"=>$keuanganTotal
            ]);
        }
        else
        {
            return response()->json([
                "status"=>false,
                "code"=>500,
                "message"=>"id transaksi gagal ditampilkan"
            ]);
        }
    }
}
