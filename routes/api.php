<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/register','API\ApiRegister@register');
Route::post('/login','API\ApiLogin@login');
Route::get('/show_obat_all','API\ApiObat@showObat');
Route::get('/show_tanggal','API\ApiPegawai@tanggalAbsensi');
Route::get('/bulan_keuangan','API\ApiKeuangan@bulanKeuangan');
Route::get('/keuangan/{tahun}/{bulan}','API\ApiKeuangan@Keuangan');
