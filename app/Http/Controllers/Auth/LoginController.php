<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if($request->username != null)
        {
            $user = User::find($request->username);
            if(Hash::check($request->password, $user->password ))
            {
                return view('home');
            }
        }
    }
}
