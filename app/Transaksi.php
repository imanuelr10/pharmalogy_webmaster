<?php
/**
 * Created by PhpStorm.
 * User: nathanael79
 * Date: 12/12/18
 * Time: 11:14 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';
    public $timestamps = false;
    protected $fillable =
        [
            'no_transaksi',
            'tanggal',
            'waktu',
            'category_transaksi',
            'supplier_id',
            'toko_id',
            'user_id'
        ];

}
