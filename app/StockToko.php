<?php
/**
 * Created by PhpStorm.
 * User: nathanael79
 * Date: 12/13/18
 * Time: 5:23 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class StockToko extends Model
{
    public $timestamps = false;
    protected $table = 'stok_toko';
    protected $fillable =
        [
          'produk_id',
          'stok',
          'expired_date',
          'toko_id'
        ];

}
