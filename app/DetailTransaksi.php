<?php
/**
 * Created by PhpStorm.
 * User: nathanael79
 * Date: 12/11/18
 * Time: 9:46 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class DetailTransaksi extends Model
{
    protected $table = 'detail_transaksi';
    public $timestamps = false;
    protected $fillable =
        [
            'transaksi_id',
            'produkstok_id',
            'jumlah',
            'harga',
            'total'
        ];

}
